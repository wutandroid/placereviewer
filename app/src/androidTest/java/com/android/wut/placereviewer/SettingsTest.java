package com.android.wut.placereviewer;

import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import android.test.suitebuilder.annotation.LargeTest;

import com.android.wut.placereviewer.view.main.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;


/**
 * Created by soive on 16.06.2016.
 */
@RunWith(MockitoJUnitRunner.class)
@LargeTest
public class SettingsTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void goToSettings() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        SystemClock.sleep(500);
        onView(withText("Ustawienia")).perform(click());
        SystemClock.sleep(500);
    }

    @Test
    public void settingsItemsVisible() {
        onView(withId(R.id.user_password)).check(matches(isDisplayed()));
        onView(withId(R.id.user_email)).check(matches(isDisplayed()));
        onView(withId(R.id.user_login)).check(matches(isDisplayed()));
        onView(withText("Notyfikacje:")).check(matches(isDisplayed()));

    }
}
